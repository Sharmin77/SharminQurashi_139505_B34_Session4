<?php
echo substr_replace("Hello","world",0);
echo "<br>";
echo "<br>";
?>

<?php
echo substr_replace("Hello world","earth",6);
echo "<br>";
echo "<br>";
?>

<?php
echo substr_replace("Hello world","earth",-5);
echo "<br>";
echo "<br>";
?>

<?php
echo substr_replace("world","Hello ",0,0);
echo "<br>";
echo "<br>";
?>

<?php
$replace = array("1: AAA","2: AAA","3: AAA");
echo implode("<br>",substr_replace($replace,'BBB',3,3));
echo "<br>";
echo "<br>";
?>
