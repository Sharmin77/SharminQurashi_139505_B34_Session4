<?php
echo substr_compare("Hello world","Hello world",0);
echo "<br>";
echo "<br>";
?>


<?php
echo substr_compare("Hello world!","Hello world!",0)."<br>"; // the two strings are equal
echo substr_compare("Hello world!","Hello",0)."<br>"; // string1 is greater than string2
echo substr_compare("Hello world!","Hello world! Hello!",0)."<br>"; // string1 is less than string2
echo "<br>";
echo "<br>";
?>


<?php
echo substr_compare("world","or",1,2)."<br>";
echo substr_compare("world","ld",-2,2)."<br>";
echo substr_compare("world","orl",1,2)."<br>";
echo substr_compare("world","OR",1,2,TRUE)."<br>";
echo substr_compare("world","or",1,3)."<br>";
echo substr_compare("world","rl",1,2)."<br>";
?>
